
export const validarForm=(form)=>{
    let valid = true;
    let _F = {...form};
    for (let key of Object.keys(_F)){
        if(_F[key].value.trim()==="" && _F[key].required){
            _F[key].error=true;
            valid=false;
        }
    }
    return {valid,_F}
}