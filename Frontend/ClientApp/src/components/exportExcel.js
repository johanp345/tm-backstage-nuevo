import React, { Component } from 'react';
import ReactExport from "react-data-export";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

export default class DownloadExcel extends React.Component {
    constructor(props){
      super(props);
      this.state={
        headers:[],
        data:this.props.data||[],
      }
    }
    componentDidMount(){
        let HeaderStyle=[]
        let inputs=[]
        this.props.headers.map(item=>{
            HeaderStyle.push({
                title:item.label,
                style:{
                    font:{
                        sz:10,
                        name:'Arial',
                        bold:true,
                        fgColor: {rgb: "FFCCEEFF"}
                    },
                    alignment:{
                        horizontal:'center',
                        vertical:'center',
                    }
                }
            })
        })
        this.props.data.map(item=>{
            let aux=[]
            for (const prop in item){
                aux.push({
                    value:item[prop],
                    style:{
                        font:{
                            name:'Arial',
                            sz:10,
                        }
                    }
                })
            }
            inputs.push(aux)
        })
        this.setState({headers:HeaderStyle,data:inputs})
    }
    componentDidUpdate(prevProps,prevState){
        if(prevProps!=this.props){
            let HeaderStyle=[]
            let inputs=[]
            this.props.headers.map(item=>{
                HeaderStyle.push({
                    title:item.label,
                    style:{
                        font:{
                            sz:10,
                            name:'Arial',
                            bold:true,
                            fgColor: {rgb: "FFCCEEFF"}
                        },
                        alignment:{
                            horizontal:'center',
                            vertical:'center',
                        }
                    }
                })
            })
            this.props.data.map(item=>{
                let aux=[]
                for (const prop in item){
                    aux.push({
                        value:item[prop],
                        style:{
                            font:{
                                name:'Arial',
                                sz:10,
                            }
                        }
                    })
                }
                inputs.push(aux)
            })
          this.setState({
            headers: HeaderStyle,
            data: inputs
          });
        }
    }
    render() {
        return (
            <ExcelFile element={<button className='descargar-excel align-items-center justify-content-center mb-4 d-flex'> <i className="icon-logout"></i>Descargar CSV </button>} filename='Ventas'>
                <ExcelSheet 
                    dataSet={[
                        {
                            columns:this.state.headers,
                            data:this.state.data
                        }
                    ]} 
                    name="Total Ventas"
                >
                    {/*
                      this.state.headers.map((item,key) => {
                        return <ExcelColumn key={key} label={item.label} value={item.value}/>
                      })
                    //*/}
                </ExcelSheet>
            </ExcelFile>
        );
    }
}