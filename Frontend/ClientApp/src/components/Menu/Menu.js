import React, { useState,useEffect } from 'react';
import { withTranslation } from 'react-i18next';
import {Link} from 'react-router-dom';
import logo from '../../assets/images/logo.svg';
import logoM from '../../assets/images/logo-movil.svg';
import menu from './menuItems';

//const cookies = new Cookies();
const Home = (props)=> {
  const {t} = props;
  //const [Loading, setLoading] = useState(false)
  useEffect(()=>{
    //console.log(menu);
  },[menu])
  
    return (
      <section id="menuL" className="">
        <figure>
          <img src={logo} className="logo-menu-g"/>
          <img src={logoM} className="logo-menu-m"/>
        </figure>
        {
          menu.map(item=>{
            return <Link key={item.url} to={item.url} className='itemMenu'>
                      <p>
                        <i className={item.icono}></i>
                        <span>{t(item.nombre)}</span>
                      </p>
                    </Link>
          })
        }
      </section>
    );
}

export default withTranslation()(Home)