import React from 'react';
import Menu from './Menu';

export const Layout = (props) => {
  const {children}= props 
  const layout = props.layout?props.layout:'completo';
    return (
      <>
          {(layout==='completo')?
            <div className='layoutCompleto w-100'>
                {children}
            </div>
            :
            <div className='layoutMenuLeft w-100 h-100'>
                <aside className="wrapMenuLeft">
                  <Menu/>
                </aside>
                <section className="wrapContentRight">
                  
                  {children}  
                </section>
            </div>
          }
      </>
    );
}
