import React, { useEffect, useState } from 'react';
import avatarImg from '../../assets/images/avatar.svg'
export default ({ user }) => {
  const [showInfo, setShowInfo] = useState(false);
  useEffect(() => {}, [user]);
  const close = () => {
    setTimeout(() => {
      if (showInfo) {
        setShowInfo(false);
      }
    }, 100);
  };
  const logout = (e) => {
    e.preventDefault();
    sessionStorage.clear();
    window.location.href = '/login/';
  };
  return (
    <div className="wrap-avatar">
      {showInfo && (
        <span
          onClick={() => setShowInfo(false)}
          style={{
            position: 'fixed',
            height: '100%',
            width: '100%',
            zIndex: 9,
            left: 0,
            top: 0,
          }}
        ></span>
      )}
      <img
        style={{ zIndex: '10' }}
        src={user.Imagen ? `/images/${user.Imagen}` : avatarImg}
        className="avatarUser"
        onClick={() => setShowInfo(!showInfo)}
      />

      {showInfo && (
        <section id="info-user" style={{ zIndex: '10' }}>
          <img
            className="mx-auto mb-2"
            style={{ width: '80px', display: 'block' }}
            src={user.Imagen ? `/images/${user.Imagen}` : avatarImg}
          />
          <span className="d-block w-100 text-black bold text-center mb-3">
            {user.Login}
          </span>
          <a
            href="/usuario"
            className="d-flex align-items-center text-grey mb-3"
          >
            {' '}
            <i className="text-grey icon-changeP mr-2 d-block"></i>{' '}
            <span className="d-block">Cambiar contraseña</span>
          </a>
          <a
            href="#"
            onClick={logout}
            className="d-flex align-items-center text-grey mb-4"
          >
            {' '}
            <i className="text-grey icon-logout mr-2 d-block"></i>{' '}
            <span className="d-block">Salir</span>
          </a>
        </section>
      )}
    </div>
  );
};
