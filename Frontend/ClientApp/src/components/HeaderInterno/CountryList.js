import React, { useState, useEffect } from 'react';
import ReactCountryFlag from 'react-country-flag';

const CountryList = (props) => {
  const { countries, country, type,setGlobalConfig } = props;
  const [selectedCountry, setSelectedCountry] = useState({});
  const [show, setShow] = useState(false);
  useEffect(() => {
    setShow(type && type === 'menu' ? false : true);
  }, [type]);

  useEffect(() => {
    if (selectedCountry.ID) {
      if (props.setSelectedCountry) props.setSelectedCountry(selectedCountry);
      setGlobalConfig({UserCountry:selectedCountry})
      sessionStorage.setItem('user-country', JSON.stringify(selectedCountry));
    }
  }, [selectedCountry]);
  useEffect(() => {
    if (country && country.ID) {
      setSelectedCountry(country);
    }
  }, [country]);
  const header = () => {
    return (
      type &&
      selectedCountry &&
      selectedCountry.ID &&
      type === 'menu' && (
        <>
          {show && (
            <span
              onClick={() => setShow(false)}
              style={{
                position: 'fixed',
                height: '100%',
                width: '100%',
                zIndex: 9,
                left: 0,
                top: 0,
              }}
            ></span>
          )}
          <span
            onClick={() => {
              setShow((show) => !show);
            }}
            className="country-sel"
          >
            <ReactCountryFlag
              countryCode={selectedCountry.Language_Region.split('-')[1]}
              style={{
                width: '2em',
                height: '2em',
                marginRight: '10px',
                marginLeft: '10px',
              }}
              svg
            />
            {selectedCountry.NombreEspanol}
            <i className={`icon-drop ${show ? 'rotate' : ''}`}></i>
          </span>
        </>
      )
    );
  };
  return (
    <>
      {header()}
      <ul className="country-list" style={{ display: show ? 'block' : 'none' }}>
        {countries && countries.length > 0
          ? countries
              .filter(
                (p) =>
                  (type === 'menu' &&
                    selectedCountry &&
                    p.ID != selectedCountry.ID) ||
                  !type ||
                  type === 'list'
              )
              .map((p, index) => (
                <li
                  className={`${p.ID === selectedCountry.ID ? 'selected' : ''}`}
                  key={index}
                  onClick={() => {
                    if (type === 'menu') setShow(false);
                    setSelectedCountry(p);
                  }}
                >
                  <ReactCountryFlag
                    countryCode={p.Language_Region.split('-')[1]}
                    style={{
                      width: '2em',
                      height: '2em',
                      marginRight: '10px',
                      marginLeft: '10px',
                    }}
                    svg
                  />
                  {p.NombreEspanol}
                </li>
              ))
          : null}
      </ul>
    </>
  );
};
export default CountryList;
