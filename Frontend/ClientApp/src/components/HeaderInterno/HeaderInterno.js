import React, { useState, useEffect } from 'react';
import Notify from './Notifies';
import Avatar from './Avatar';
import CountryList from './CountryList';

const HeaderInterno = ({global,t,title,setGlobalConfig}) => {
  const [showT, setShowT] = useState(null);
  const userCountry = JSON.parse(sessionStorage.getItem('user-country')) ;
  useEffect(() => {
    if(userCountry && !global.UserCountry){
      setGlobalConfig({UserCountry:userCountry})
    }
  }, []);

  const handleToggle = (val) => {
    setShowT(val);
  };
  return (
    <header id="headerInterno" className="d-flex justify-content-between">
      <div className="container-fluid d-flex align-items-center">
        <div className="headerModulo" style={{ position: 'relative' }}>
          <p className="titulo text-black mb-0">{title.toUpperCase()}</p>
        </div>
        <div className="wrap-nofify ml-auto d-flex align-items-center">
          <div className="d-none d-md-block mr-3">
            <div className="wrap-countries">
              <CountryList
                countries={global.PaisesConEventos}
                type="menu"
                country={global.UserCountry?global.UserCountry :null}
                setGlobalConfig={setGlobalConfig}
              />
            </div>
          </div>
          <div className="d-none d-md-block mr-5">
            <Notify
              notificaciones={[]}
              cOpen={showT}
              handleToggle={handleToggle}
            />
          </div>
          <div className="d-none d-md-block">
            <Avatar user={global} />
          </div>
        </div>
      </div>
    </header>
  );
};

export default HeaderInterno;
