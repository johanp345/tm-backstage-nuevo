import React, { useEffect, useState } from 'react';

export default () => {
  const [showInfo, setShowInfo] = useState(false);
  const [Notificaciones, setNotificaciones] = useState([]);
  
  const close = () => {
    setTimeout(() => {
      if (showInfo) {
        setShowInfo(false);
      }
    }, 100);
  };

  return (
    <div className="wrap-notify">
      {showInfo && (
        <span
          onClick={() => setShowInfo(false)}
          style={{
            position: 'fixed',
            height: '100%',
            width: '100%',
            zIndex: 9,
            left: 0,
            top: 0,
          }}
        ></span>
      )}

      <i 
          className={`text-grey ${showInfo?'icon-cancel-circle':'icon-notificaciones'}`} 
          onClick={()=>setShowInfo(!showInfo)}
      />

      {showInfo && (
        <div className="notify-content">
              {
                (Notificaciones.length===0)?
                <div className="noNotify w-100 text-center"><span style={{display:'block',whiteSpace:'nowrap'}}>No hay notificaciones Nuevas</span></div>
                :<ul>
                  {
                    Notificaciones.map((item,key) => {
                      return  <li key={key} onClick={(e)=>this.toggleList(e)}></li>
                    })
                  }
                </ul>
              }
        </div>
      )}
    </div>
  );
};
