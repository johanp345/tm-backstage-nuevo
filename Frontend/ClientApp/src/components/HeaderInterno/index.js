import { connect } from "react-redux";
import HeaderInterno from "./HeaderInterno";
import { setConfig} from "../../actions/global/creators";

const mapStateToProps = (state) => ({
  global: state.global,
});

const mapDispatchToProps = (dispatch) => ({
  setGlobalConfig: (config) => dispatch(setConfig(config)),
});

export default connect(mapStateToProps, mapDispatchToProps)(HeaderInterno);
