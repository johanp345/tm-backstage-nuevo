import React, { useState,useEffect } from 'react';
import { withTranslation } from 'react-i18next';
import {Link,useHistory} from 'react-router-dom';
import logoCli from '../../assets/images/logo.svg';
import logoPowered from '../../assets/images/logo-powered.svg';
import Spinner from '../../components/Spinner';
import CountryList from './CountryList';
import {validarForm} from '../../components/Utils';
import Api from '../../services/services';

//const cookies = new Cookies();
const Login = ({global,t,setGlobalConfig})=> {
  const [Loading,setLoading]=useState(false);
  const [ShowPass,setShowPass]=useState(false);
  const [Usuario,setUsuario]=useState(false);
  const history =useHistory();
  const [Form,setForm]=useState({
    usuario:{
      value:"",
      required:true,
      error:null
    },
    password:{
      value:"",
      required:true,
      error:null
    }
  })
  const [ErrorLogin,setErrorLogin]=useState(false)

  useEffect(()=>{
    //console.log(Usuario,global);
  },[global])

  const submit=(e)=>{
    e.preventDefault();
    let {valid,_F} = validarForm(Form)
    if(!valid){
      setForm(_F);
      return 0;
    }
    setLoading(true);
    (async ()=>{
        const {status,data} = await Api.access.login(
            `${global.baseAddress}Montaje/Usuario/LoginDashboard`,
            {username:Form.usuario.value,password:Form.password.value}
        )
        
        if(status===200){
          setUsuario(data);
        }else if(status===401 || status === 400){
          setErrorLogin(true)
        }
        setLoading(false);
      }
      )()
    }
    
  const handePais=(pais)=>{
    setGlobalConfig({...Usuario,UserCountry:pais});
    sessionStorage.setItem("user-country",JSON.stringify(pais))
    sessionStorage.setItem("usuario",JSON.stringify(Usuario))
    //history.replace("/")
  }

  const handleForm=(e)=>{
    let {name,value}=e.target;
    setForm({
      ...Form,
      [name]:{...Form[name],value}
    })
  }
    return (
      <div className="d-flex align-items-stretch w-100 h-100" id="pageLogin">
        <section className="w-100 bg-login d-flex align-items-end justify-content-start">
          <img src={logoPowered} className="m-4"/>
        </section>
        <section className="w-100 d-flex align-items-center justify-content-center bg-ppal ">
            {(Usuario)?
              <div>
                <figure>
                  <img src={logoCli}/>
                </figure>
                <CountryList countries={Usuario.PaisesConEventos} handePais={handePais}/>
              </div>
            :
            <section className="wrapLogin">
              <figure>
                <img src={logoCli}/>
              </figure>
              <h2 className="f20">{t('bienvenido')}</h2>
              <form onSubmit={submit} autoComplete="new-password">
                <div className="row">
                  <div className="col-12">
                    <div className="form-group">
                      <label className="d-block">{t('formUsuario')}</label>
                      <input className="form-control" onChange={handleForm} type="text" value={Form.usuario.value} name="usuario" autoComplete="new-password"/>
                      {(Form.usuario.error)&&<span className="errorForm d-block">{t('campoRequerido')}</span>}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group">
                      <label className="d-block">{t('formClave')}</label>
                      <input className="form-control" onChange={handleForm} type={(ShowPass)?"text":"password"} value={Form.password.value} name="password" autoComplete="new-password"/>
                      <i
                        className="icon-mostrar-contrasena text-white"
                        onClick={()=>setShowPass(!ShowPass)}
                      ></i>
                      {(Form.password.error)&&<span className="errorForm d-block">{t('campoRequerido')}</span>}
                    </div>
                  </div>
                  <div className="col-12">
                    {
                      Loading?
                      <button className="btn btnGeneral d-flex align-items-center justify-content-center w-100" disabled={true}><span className="w-100"><Spinner/></span></button>
                      :
                      <button className="btn btn-block btnGeneral" disabled={global.baseAddress?false:true}><span>{t('buttonLogin')}</span></button>
                    }
                  </div>
                  {(ErrorLogin)&&<div className="col-12">
                    <span className="errorForm mt-3 d-block text-center">{t('errorLogin')}</span>
                  </div>}
                </div>
              </form>
            </section>
            }
        </section>
      </div>
    );
}

export default withTranslation()(Login)