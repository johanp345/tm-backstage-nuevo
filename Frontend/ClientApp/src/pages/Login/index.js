import { connect } from "react-redux";
import Login from "./Login";

import { getGlobalConfig,setConfig} from "../../actions/global/creators";

const mapStateToProps = (state) => ({
  global: state.global,
});

const mapDispatchToProps = (dispatch) => ({
  getGlobalConfig: () => dispatch(getGlobalConfig()),
  setGlobalConfig: (config) => dispatch(setConfig(config)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
