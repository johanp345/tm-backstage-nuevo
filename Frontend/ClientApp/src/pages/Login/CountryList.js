import React, { useState, useEffect } from 'react';
import ReactCountryFlag from 'react-country-flag';
import { setDefaults, withTranslation } from 'react-i18next';

const CountryList = ({ countries, country,t,handePais}) => {

  const [Default, setDefault] = useState(false);
  const [selectedCountry, setSelectedCountry] = useState(null);
  

  useEffect(() => {
    
  }, [selectedCountry]);
  useEffect(() => {
    if (country && country.ID) {
      setSelectedCountry(country);
    }
  }, [country]);
  
  const confirmLogin=()=>{
    if (selectedCountry) {
      handePais(selectedCountry);
    }
  }
  
  return (
    <div className="listaPaises">
      <header className="d-flex w-100 justify-content-between align-items-center">
        <span>{t("seleccionarPais")}</span>
        <i className="icon-cerrar" onClick={()=>document.location.href="/"}></i>
      </header> 
      <ul>
        {countries && countries.length > 0
          ? countries
              .map((p, index) => (
                <li
                  className={`${ !selectedCountry? "" :p.ID === selectedCountry.ID ?'selected' : ''}`}
                  key={index}
                  onClick={() => {
                    setSelectedCountry(p);
                  }}
                >
                  <ReactCountryFlag
                    countryCode={p.Language_Region.split('-')[1]}
                    style={{
                      width: '2em',
                      height: '2em',
                      marginRight: '10px',
                      marginLeft: '10px',
                    }}
                    svg
                  />
                  {p.NombreEspanol}
                </li>
              ))
          : null}
      </ul>
      <footer className="d-flex justify-content-between align-items-center"> 
          <label htmlFor="default">
            <input type="checkbox" id="default" onChange={()=>setDefault(true)} defaultChecked={Default}/>
            <span>{t("paisDefault")}</span>
          </label>
          <button className="btn bg-btn-1 btnGeneral" disabled={(selectedCountry)?false:true} onClick={()=>confirmLogin()}>{t("continuar")}</button>
      </footer>
    </div>
  );
};
export default withTranslation()(CountryList);
