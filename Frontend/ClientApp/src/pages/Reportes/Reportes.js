import React, { useEffect, useState } from 'react';
import { withTranslation } from 'react-i18next';
import Select from 'react-select';
import DatePicker from "react-datepicker";
import Spinner from '../../components/Spinner';
import HeaderInterno from '../../components/HeaderInterno';
import Api from '../../services/services';

//const cookies = new Cookies();
const Reporte = (props)=> {
  const {global,t} = props;
  const [TipoReporte,setTipoReporte]=useState(null);
  const [Reporte,setReporte]=useState(null);
  const [ShowLista,setShowLista]=useState(false);
  const [DatosIframe,setDatosIframe]=useState({ReporteID:0})

  const [OptEvento,setOptEvento]=useState([]);
  const [SelectedOptEvento,setSelectedOptEvento]=useState(null);
  const [LoadingEventos,setLoadingEventos]=useState(false);
  
  const [OptFunciones,setOptFunciones]=useState([{ value: 'All', label: 'Seleccionar Todos' }]);
  const [SelectedOptFunciones,setSelectedOptFunciones]=useState(null);
  const [LoadingFunciones,setLoadingFunciones]=useState(false);
  
  const [OptCategorias,setOptCategorias]=useState([]);
  const [SelectedOptCategorias,setSelectedOptCategorias]=useState(null);
  const [LoadingCategorias,setLoadingCategorias]=useState(false);
  
  const [OptTop,setOptTop]=useState([{ value: '20', label: '20' },{ value: '50', label: '50' },{ value: '100', label: '100' },{ value: '200', label: '200' }]);
  const [SelectedValueOptTop,setSelectedValueOptTop]=useState(null);
  
  const [StartDate, setStartDate] = useState(null);
  const [EndDate, setEndDate] = useState(null);
  
  const [iframeKey, setIframeKey] = useState(0);
  const [iframeURl, setIframeUrl] = useState('');
  const [LoadingIframe,setLoadingIframe]=useState(false);
  
  useEffect(()=>{
    //console.log(global);
    console.log(Reporte,TipoReporte);
    setIframeUrl("")
  },[Reporte,TipoReporte])

  useEffect(()=>{
    setDatosIframe({ReporteID:Reporte?Reporte.ID:0})
    if (Reporte && (Reporte.Funcion !== null || Reporte.Evento !== null)  )
    {
      setDatosIframe({ReporteID:Reporte.ID})
      setLoadingEventos(true);
      let loginInfo = {
        ID: global.ID,
        PaisID: global.UserCountry.ID,
        SeleccionMultiple : false
      };
      (async ()=>{
        let {status,data} = await Api.filtros.eventos(global.baseAddress,loginInfo)
        if(status===200){
          setOptEvento(data);
        }
        setLoadingEventos(false);
      }
      )()
    }
    if (Reporte &&  Reporte.FuncionActivos !== null  )
    {
      let loginInfo = {
        ID: global.ID,
        PaisID: global.UserCountry.ID,
        SeleccionMultiple : false
      };
      (async ()=>{
        let {status,data} = await Api.filtros.eventosActivos(global.baseAddress,loginInfo)
        if(status===200){
          setOptEvento(data);
        }
          setLoadingEventos(false);
        }
      )()
    }

    if (Reporte &&  Reporte.Evento != null  )
    {
      let loginInfo = {
        ID: global.ID,
        PaisID: global.UserCountry.ID,
        SeleccionMultiple : true
      };
      (async ()=>{
        let {status,data} = await Api.filtros.eventos(global.baseAddress,loginInfo)
        if(status===200){
          setOptEvento(data);
        }
          setLoadingEventos(false);
        }
      )()
    }
    if (Reporte &&  Reporte.eventosActivos != null  )
    {
      let loginInfo = {
        ID: global.ID,
        PaisID: global.UserCountry.ID,
        SeleccionMultiple : true
      };
      (async ()=>{
        let {status,data} = await Api.filtros.eventos(global.baseAddress,loginInfo)
        if(status===200){
          setOptEvento(data);
        }
          setLoadingEventos(false);
        }
      )()
    }
    if (Reporte && Reporte.Categoria !== null){
      setDatosIframe({ReporteID:Reporte.ID})
      setLoadingCategorias(true);
      (async ()=>{
        let {status,data} = await Api.filtros.categorias(global.baseAddress,{
          Idioma: 'ES',
        })
        if(status===200){
          setOptCategorias(data);
        }
        setLoadingCategorias(false);
      }
      )()
    }
  },[Reporte])

  const handleChangeOptEventos = (e)=>{
    setSelectedOptEvento(e);
    setLoadingFunciones(true);
    setSelectedOptFunciones([]);
    setOptFunciones([]);
    let loginInfo = {
      ID: e.value,
      UserID: global.ID,
      PaisID: global.UserCountry.ID,
    };
    (async ()=>{
      let {status,data} = await Api.filtros.funcionesActivos(global.baseAddress,loginInfo)
      if(status===200){
        setOptFunciones(data);
      }
      setLoadingFunciones(false);
    }
    )()
  }

  const handleChangeOptCategorias = (e)=>{
    setSelectedOptCategorias(e);
    let str="";
    if (e != null){
      str = "";
      for (var i = 0; i < e.length; i++) {
        if(e[i].value == 0 ){
          for (var j = 1; j < OptCategorias.length; j++) {
            if(str == ""){str += OptCategorias[j].value ;}
            else str += "," + OptCategorias[j].value
          }
        }
        else{
          if(str == ""){str += e[i].value ;}
          else str += "," + e[i].value
        }
      }
      if(e.length > 0){
        setDatosIframe({
          ...DatosIframe,
          [Reporte.Categoria]: str,
        });
      }
    }
  }
  
  const handleChangeOptFunciones = (event)=>{
    setSelectedOptFunciones(event);
    if (event != null){
      let str = "";
      for (var i = 0; i < event.length; i++) {
        if(event[i].value == 0 ){
          for (var j = 1; j < OptFunciones.length; j++) {
            if(str == ""){str += OptFunciones[j].value ;}
            else str += "," + OptFunciones[j].value
          }
        }
        else{
          if(str == ""){str += event[i].value ;}
          else str += "," + event[i].value
        }
      }
      console.log(str);
      if(event.length > 0){
        setDatosIframe({
          ...DatosIframe,
          [Reporte.Funcion]: str,
        });
      }
    }

  }

  const handleChangeOptSoloEventos = (event) => {
    setSelectedOptEvento(event);
    if (event != null){
      let str = "";
      for (var i = 0; i < event.length; i++) {
        if(event[i].value == 0 ){
          for (var j = 1; j < OptEvento.length; j++) {
            if(str == ""){str += OptEvento[j].value ;}
            else str += "," + OptEvento[j].value
          }
        }
        else{
          if(str == ""){str += event[i].value ;}
          else str += "," + event[i].value
        }
      }
      if(event.length > 0){
        setDatosIframe({
          ...DatosIframe,
          [Reporte.Evento]: str,
        });
      }
    }

  };

  const handleChangeStartDate = (date) => {
    setStartDate(date);
    setDatosIframe({
      ...DatosIframe,
      [Reporte.FechaInicio ]: formatDate(date),
    });

  }
  const handleChangeEndDate = (date) => {
    setEndDate(date);
    setDatosIframe({
      ...DatosIframe,
      [Reporte.FechaFin]: formatDate(date),
    });
  }
  const formatDate = (date) => {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
  }

  const cargarIframe = ()=>{
    setLoadingIframe(true)
    setIframeUrl('')
    let parameters=[]
    let cont = true;
    if (Reporte.Pais != null){
      parameters.push({
              Key: Reporte.Pais,
              Value: global.UserCountry.ID,
            });
            cont = true;
    }
    if (Reporte.Funcion != null ) {
      if (DatosIframe[Reporte.Funcion] == '' || DatosIframe[Reporte.Funcion] == null){
        cont = false;
      }
      else{
        cont = true;
      parameters.push({
        Key: Reporte.Funcion,
        Value: DatosIframe[Reporte.Funcion],
      });
    }
    };
    if (Reporte.Evento != null ) {
      if (DatosIframe[Reporte.Evento] == '' || DatosIframe[Reporte.Evento] == null){
        cont = false;
        }
        else{
          cont = true;
      parameters.push({
        Key: Reporte.Evento,
        Value: DatosIframe[Reporte.Evento],
      });
    }
    };
    if (Reporte.FechaFin != null ){
      if (EndDate == '' || EndDate == null){

        cont = false;
        }
        else{
          cont = true;
          parameters.push({
            Key: Reporte.FechaFin,
            Value: formatDate(EndDate),
          });
        }
    };
    if (Reporte.FechaInicio != null ){
      if (StartDate == '' || StartDate == null){
        cont = false;
      }
      else{
        cont = true;
        parameters.push({
          Key: Reporte.FechaInicio,
          Value:  formatDate(StartDate),
        });
      }
    };
    if (Reporte.Idioma != null ){
      cont = true;
      parameters.push({
        Key: Reporte.Idioma,
        Value:'es',
      });
    };
    if (Reporte.Categoria != null ) {
      if (DatosIframe[Reporte.Categoria] == '' || DatosIframe[Reporte.Categoria] == null){
        cont = false;
      }
      else{
        cont = true;
        parameters.push({
          Key: Reporte.Categoria,
          Value: DatosIframe[Reporte.Categoria],
        });
      }
    };
    if (Reporte.Empresa){
      cont = true;
      parameters.push({
        Key: Reporte.Empresa,
        Value: global.EmpresaID,
      });
    }
    if (Reporte.Top){
      cont = true;

      parameters.push({
        Key: Reporte.Top,
        Value: SelectedValueOptTop.value,
      });
    }
    //console.log('https://backstage.ticketmundo.com/intra/Reports/ReportViewerIframe.aspx?id=' + DatosIframe['ReporteID'] +'&json=' + JSON.stringify(parameters));
    setIframeUrl('https://backstage.ticketmundo.com/intra/Reports/ReportViewerIframe.aspx?id=' + DatosIframe['ReporteID'] +'&json=' + JSON.stringify(parameters))
    //setIframeUrl('/intra/Reports/ReportViewerIframe.aspx?id=' + datos['ReporteID'] +'&json=' + JSON.stringify(parameters))
    //console.log(iframeURl);
  }

    return (
      <section id="pageReportes">
        <HeaderInterno title={`Reportes ${TipoReporte?'/ '+TipoReporte.Nombre:''}`}/>
        <div className="wrapContent d-flex w-100">
          {
            (TipoReporte!==null)&&
            <div className={`listaReportes ${ShowLista?'show':''}`}>
              {
                (!ShowLista)&&<span onClick={()=>setShowLista(true)}><i className="icon-flecha-lista openL"></i></span>
              }
              <ul>
                {
                  TipoReporte.PermisosReportes.map((re)=>{
                    return <li key={re.ID} 
                              className="d-flex w-100 align-items-center"
                              onClick={()=>{setReporte(re);setShowLista(false)}}
                            >
                              <i className="icon-file"></i><span>{re.Nombre}</span>
                            </li>
                  })
                }
                <li className="d-flex w-100 align-items-center" onClick={()=>setShowLista(false)}>
                  <i className="icon-flecha-lista "></i><span>Ocultar Lista</span>
                </li>
              </ul>
            </div>
          }
          <div className={(TipoReporte===null && global.gruposReportes)?"container-fluid":"w-100"}>
            {
              (TipoReporte===null && global.gruposReportes)?
              <section className="wrapTipos d-flex justify-content-center w-100">
                {
                  global.gruposReportes.map((r)=>{
                    return <div key={r.ID} 
                            onClick={()=>{setTipoReporte(r);setShowLista(true)}}
                            className="cardTipo d-flex flex-wrap align-items-center justify-content-center">
                              <i className={`icon-reporte-${r.Nombre.toLowerCase()}`}></i>
                              <div className="descripcion w-100">
                                <p className="">{r.Nombre}</p>
                                <span className="">Reporte de {r.Nombre}</span>
                              </div>
                          </div>
                  })
                }
              </section>
              :
              <section className="h-100 d-flex align-items-start wrapFilter flex-wrap">
                  {(Reporte===null)?
                    <span className="align-self-center cTexto2 seleccioneRep">
                      <i className="icon-file"></i>
                      <p>Por favor seleccione el tipo de reporte</p>
                    </span>
                    :
                    <>
                    <section className="filters w-100">
                      <div className="row">
                        <div className="col-12">
                          <h2>{Reporte.Nombre}</h2>
                        </div>
                        <div className="col-12 col-md-8 col-xl-9">
                          <div className="row">
                            {/*Input Eventos*/}
                            { (Reporte.Funcion)&&
                              <div className="col-6 col-md-4 rwInput">
                                <div className=" w-100">
                                  <label>Eventos</label>
                                  <Select options={OptEvento}
                                      // isMulti
                                      value = {SelectedOptEvento}
                                      placeholder="Seleccionar"
                                      isLoading = {LoadingEventos}
                                      loadingMessage={() => "Buscando"}
                                      noOptionsMessage={() => "No hay resultados"}
                                    //  closeMenuOnSelect={false}
                                    //  name="colors"
                                    className="basic-multi-select selectReact"
                                    onChange={handleChangeOptEventos}            // className="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 mb-5"
                                    // dropdownIndicator
                                    />
                                </div>
                              </div>
                            }
                            {/*Input funciones (depende de eventos)*/}
                            { (Reporte.Funcion)&&
                              <div className="col-6 col-md-4 rwInput">
                                <div className=" w-100">
                                  <label>Funciones</label>
                                  <Select options={OptFunciones}
                                      isMulti
                                      closeMenuOnSelect={false}
                                      value = {SelectedOptFunciones}
                                      placeholder="Seleccionar"
                                      isLoading = {LoadingFunciones}
                                      loadingMessage={() => "Buscando"}
                                      noOptionsMessage={() => "No hay resultados"}
                                      className="basic-multi-select selectReact"
                                      onChange={handleChangeOptFunciones}
                                      isDisabled={OptFunciones.length===1}
                                    />
                                </div>
                              </div>
                            }
                            {/*INPUT categoria*/}
                            { (Reporte.Categoria)&&
                              <div className="col-6 col-md-4 rwInput">
                                <div className=" w-100">
                                  <label>Categorías</label>
                                  <Select options={OptCategorias}
                                    isMulti
                                    value={SelectedOptCategorias}
                                    placeholder="Seleccionar"
                                    loadingMessage={() => "Buscando"}
                                    noOptionsMessage={() => "No hay resultados"}
                                    isLoading = {LoadingCategorias}
                                    className="basic-multi-select selectReact"
                                    onChange={handleChangeOptCategorias}
                                  />
                                </div>
                              </div>
                            }
                            {/*INPUT evento*/}
                            { (Reporte.Evento)&&
                              <div className="col-6 col-md-4 rwInput">
                                <div className=" w-100">
                                  <label>Eventos</label>
                                  <Select options={OptEvento}
                                    isMulti
                                    value = {SelectedOptEvento}
                                    closeMenuOnSelect={false}
                                    placeholder="Seleccionar"
                                    loadingMessage={() => "Cargando..."}
                                    noOptionsMessage={() => "No hay resultados "}
                                    className="basic-multi-select selectReact"
                                    onChange={handleChangeOptSoloEventos}
                                  />
                                </div>
                              </div>
                            }

                            {/*INPUT top*/}
                            { (Reporte.Top)&&
                              <div className="col-6 col-md-4 rwInput">
                                <div className=" w-100">
                                  <label>Top</label>
                                  <Select options={OptTop}
                                    value={SelectedValueOptTop}
                                    placeholder="Seleccionar"
                                    isSearchable = {false}

                                    defaultValue={{ value: 20, label: "20" }}
                                    className="basic-multi-select selectReact"
                                    onChange={e => setSelectedValueOptTop(e)}
                                  />
                                </div>
                              </div>
                            }
                            
                          </div>
                          <div className="row">
                            {/*Input fecha inicio*/}
                            { (Reporte.FechaInicio)&&
                              <div className="col-6 col-md-4 rwInput">
                                <div className=" w-100">
                                  <label>Fecha Inicio</label>
                                  <DatePicker
                                    placeholderText=" Seleccione..."
                                    selected={StartDate}
                                    onChange={handleChangeStartDate}
                                    className="datePicker"
                                    // customInput={<CustomInicialInput />}
                                    style={{paddingtop:"9px"}} />
                                </div>
                              </div>
                            }
                            {/*Input fecha Fin*/}
                            { (Reporte.FechaFin)&&
                              <div className="col-6 col-md-4 rwInput">
                                <div className=" w-100">
                                  <label>Fecha Fin</label>
                                  <DatePicker
                                    placeholderText=" Seleccione..."
                                    selected={EndDate}
                                    onChange={handleChangeEndDate}
                                    className="datePicker"
                                    // customInput={<CustomInicialInput />}
                                    style={{paddingtop:"9px"}} />
                                </div>
                              </div>
                            }
                          </div>
                        </div>
                        <div className="col-12 col-md-4 col-xl-3 rwInput d-flex">
                            {
                              LoadingIframe?
                              <button className="btn btnGeneral2 d-flex align-items-center justify-content-center w-100 mt-auto" disabled={true}><span className="w-100"><Spinner/></span></button>
                              :
                              <button className="btn btnGeneral2 btn-block mt-auto" onClick={cargarIframe}><span>Buscar</span></button>
                            }
                        </div>
                      </div>
                    </section>
                    { (iframeURl.trim()!=="")&&
                      <div className="container-selected-report w-100">
                        <iframe  key={iframeKey} src={iframeURl} onLoad={e=> { setLoadingIframe(false)}}  scrolling="no" frameBorder="0" style={(iframeURl.trim()==="")?{}:{width: '100%', height :'550px'}}></iframe>
                      </div>
                    }
                    </>
                  }
              </section>
            }
          </div>
        </div>
      </section>
    );
}

export default withTranslation()(Reporte)