import React,{useState,useEffect} from 'react';
import { useTable, usePagination } from 'react-table';
import * as moment from 'moment';
import 'moment/locale/es';
import DatePicker,{registerLocale, setDefaultLocale} from "react-datepicker";
import es from 'date-fns/locale/es';
import DownloadExcel from '../../components/exportExcel';
moment.locale('es')
registerLocale('es', es)
setDefaultLocale('es')

const TablaVentas = ({ventas})=>{
    const [rowsFilters,setRowsFilters]=useState(ventas)
    
    const [fecha,setFecha]=useState(null);
    const [HeaderExc,setHeaderExc]=useState([]);
    const [DataExc,setDataExc]=useState([]);
    const [search,setSearch]=useState('');
    
    useEffect(() => {
      console.log(ventas);
      //console.log(nombreVideo);
      setRowsFilters(ventas)
      if(ventas.length>0){
        getDatosExcel(ventas)
      }
    }, [ventas]);


    const getDatosExcel= (json) =>{
      let rows = [];
      let h = [
        { label: 'ORDEN', value: 'Orden' },
        { label: 'VIDEO', value: 'Video' },
        { label: 'COMPRADOR', value: 'Comprador' },
        { label: 'FECHA', value: 'Fecha' },
        { label: 'ESTATUS', value: 'Estatus' },
        { label: 'MONTO', value: 'Monto' },
        { label: 'TOTAL', value: 'Total' }
      ]

      json.map(item => {
        const row = {
          Orden: item.id,
          Video:item.NombreVideo,
          Comprador: item.customer_email +"("+ item.customer_name +")" ?? '',
          Fecha: moment.utc(item.Fecha).format("MMMM DD, yyyy"),
          Estatus: item.pagado?"Pagado":"Cupón",
          Monto: item.amount,
          Total: item.total,
        };
        rows.push(row);
      })
      setHeaderExc(h)
      setDataExc(rows)
    }

    const columns = React.useMemo(
        () => [
            {
                Header: 'Evento',
                id: 'evento',
                Cell:({row})=>{
                 return row.original.Evento?row.original.Evento:""
                }
            },
            {
                Header: '(%)',
                id: 'porcentaje',
                Cell:({row})=>{
                  return row.original.Porcentaje?row.original.Porcentaje:""
                }
            },
            {
                Header: 'Estado',
                id: 'estado',
                Cell:({row})=>{
                  return row.original.Estado?row.original.Estado:""
                }
            },
            {
                Header: 'Tickets',
                id: 'tickets',
                Cell:({row})=>{
                  return row.original.Tickets?row.original.Tickets:""
                }
            },
            
        ],
        []
    )
    


    const handleFecha = (value)=>{
      setFecha(value)
      if(value==null){
        setRowsFilters(ventas)
      }else{
        setRowsFilters(ventas.filter(p=>p.fecha==moment.utc(value).format("DD/MM/yyyy") ))
      }
    }

    const handleSearch = (value)=>{
      setSearch(value)
      if(value.trim()==""){
        setRowsFilters(ventas)
      }else{
        setRowsFilters(ventas.filter(p=>p.customer_name.toLowerCase().indexOf(value.toLowerCase())>=0 ))
      }
    }
        const data = React.useMemo(() => rowsFilters, [rowsFilters])

        return (
            <section className='wrapTablaVentas'>
                {/*<div className='mt-1 d-flex flex-wrap filtros-ventas'>
                  <div className='wrappers mb-5 mr-5'>
                    <i className='icon-calendar'></i>
                    <DatePicker
                      selected={fecha}
                      className=""
                      placeholderText="Fecha"
                      onChange={date => handleFecha(date)}
                      showPopperArrow={false}
                      dateFormat="dd/MM/yyyy"
                    />
                  </div>
                  <div className='wrappers mb-5'>
                    <i className='icon-search'></i>
                    <input className='search' placeholder="Nombre del cliente" value={search} onChange={(e)=>handleSearch(e.target.value)}/>
                  </div>
                  <span className='d-flex ml-auto' >
                    {(ventas.length>0)&&<DownloadExcel headers={HeaderExc}
                                data={DataExc} />}
                  </span>
                </div>*/}
                <Tabla columns={columns} data={data} />
            </section>
        )
}

const Tabla=({ columns, data })=> {
    // Use the state and functions returned from useTable to build your UI
    const {
      getTableProps,
      getTableBodyProps,
      headerGroups,
      prepareRow,
      page, // Instead of using 'rows', we'll use page,
      // which has only the rows for the active page
  
      // The rest of these things are super handy, too ;)
      canPreviousPage,
      canNextPage,
      pageOptions,
      pageCount,
      gotoPage,
      nextPage,
      previousPage,
      setPageSize,
      state: { pageIndex, pageSize },
    } = useTable({
        columns,
        data,
        initialState: { pageIndex: 0,pageSize:10 }
      },
      usePagination
    )
    
    // Render the UI for your table
    let Total =0;
    return (
      <div className=''>
        
        <div className='table-responsive'>
         <table {...getTableProps()} className='table table-borderless tabla-ventas'>
          <thead>
            {headerGroups.map(headerGroup => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map(column => (
                  <th {...column.getHeaderProps()}>{column.render('Header')}</th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {page.map((row, i) => {
              Total=parseFloat(Total)+parseFloat(row.original.total);
              prepareRow(row)
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map(cell => {
                    return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                  })}
                </tr>
              )
            })}
          </tbody>
        </table>
        <div className='rowTotal d-flex justify-content-between py-4'>
          <span className=''>Total</span>
          <span className=''>${/*parseFloat(Total).toFixed(2)*/ "00"}</span>
        </div>
          {<div className="pagination w-100 d-flex justify-content-center mb-5">

            {canPreviousPage&&<button onClick={() => previousPage()} >
              <i className='icon-arrowhead-left'></i>
            </button>}
            
            <span className='d-flex'>
            {(pageIndex + 3>pageOptions.length && pageIndex - 4>=0)&& <span className='numberPag' onClick={()=>gotoPage(pageIndex+2)}>{pageIndex-3}</span>}
            {(pageIndex + 2>pageOptions.length && pageIndex - 3>=0)&& <span className='numberPag' onClick={()=>gotoPage(pageIndex+1)}>{pageIndex-2}</span>}
            {(pageIndex - 2>=0)&& <span className='numberPag' onClick={()=>gotoPage(pageIndex-2)}>{pageIndex-1}</span>}
            {(pageIndex - 1>=0)&& <span className='numberPag' onClick={()=>gotoPage(pageIndex-1)}>{pageIndex}</span>}
            {<span className='numberPag activo'>{pageIndex+1}</span>}
            {(pageIndex + 2<=pageOptions.length)&& <span className='numberPag' onClick={()=>gotoPage(pageIndex+1)}>{pageIndex+2}</span>}
            {(pageIndex + 3<=pageOptions.length)&& <span className='numberPag' onClick={()=>gotoPage(pageIndex+2)}>{pageIndex+3}</span>}
            {(pageIndex - 1<0 && pageIndex + 4<=pageOptions.length)&& <span className='numberPag' onClick={()=>gotoPage(pageIndex+3)}>{pageIndex+4}</span>}
            {(pageIndex - 2<0 && pageIndex + 5<=pageOptions.length)&& <span className='numberPag' onClick={()=>gotoPage(pageIndex+4)}>{pageIndex+5}</span>}
            </span>

            {canNextPage&&<button onClick={() => nextPage()}>
              <i className='icon-arrowhead-right'></i>
            </button>}
          </div>}
        </div>
      </div>
    )
}

export default TablaVentas;