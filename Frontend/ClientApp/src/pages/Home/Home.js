import React, { useEffect } from 'react';
import { withTranslation } from 'react-i18next';
import HeaderInterno from '../../components/HeaderInterno';
import Actividad from './Actividad';
import TablaVentas from './TablaVentas';
//const cookies = new Cookies();
const ventas=[
  {
    Evento:"Nombre del evento",
    Porcentaje:"00.00%",
    Estado:"En venta",
    Tickets:"00"
  },
  {
    Evento:"Nombre del evento",
    Porcentaje:"00.00%",
    Estado:"En venta",
    Tickets:"00"
  },
  {
    Evento:"Nombre del evento",
    Porcentaje:"00.00%",
    Estado:"En venta",
    Tickets:"00"
  },
  {
    Evento:"Nombre del evento",
    Porcentaje:"00.00%",
    Estado:"En venta",
    Tickets:"00"
  },
  {
    Evento:"Nombre del evento",
    Porcentaje:"00.00%",
    Estado:"En venta",
    Tickets:"00"
  },
  {
    Evento:"Nombre del evento",
    Porcentaje:"00.00%",
    Estado:"En venta",
    Tickets:"00"
  },
  {
    Evento:"Nombre del evento",
    Porcentaje:"00.00%",
    Estado:"En venta",
    Tickets:"00"
  },
  {
    Evento:"Nombre del evento",
    Porcentaje:"00.00%",
    Estado:"En venta",
    Tickets:"00"
  },
  {
    Evento:"Nombre del evento",
    Porcentaje:"00.00%",
    Estado:"En venta",
    Tickets:"00"
  },
  {
    Evento:"Nombre del evento",
    Porcentaje:"00.00%",
    Estado:"En venta",
    Tickets:"00"
  },
  {
    Evento:"Nombre del evento",
    Porcentaje:"00.00%",
    Estado:"En venta",
    Tickets:"00"
  },
]
const Home = (props)=> {
  const {global,t} = props;
  useEffect(()=>{
    console.log(props,global);
  })
    return (
      <>
        <HeaderInterno title="Inicio" />
        <div className="container-fluid" id="pageHome">
          <div className="row">
            <div className="col-12 col-lg-8">
              <div className="top d-flex align-items-center">
                <div className="location">
                  <p className="mb-0 text-blue">Ticketmundo USA</p>
                  <span>Ventas desde 00/00/0000 hasta 00/00/0000</span>
                </div>
                <div className="filter ml-auto">
                  <div className="custom-control custom-radio custom-control-inline mr-4">
                    <input type="radio" defaultChecked={true} id="customRadioInline1" name="tipoReporte" className="custom-control-input"/>
                    <label className="custom-control-label" htmlFor="customRadioInline1">Tickets</label>
                  </div>
                  <div className="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="customRadioInline2" name="tipoReporte" className="custom-control-input"/>
                    <label className="custom-control-label" htmlFor="customRadioInline2">USD</label>
                  </div>
                </div>
                <div className="rango cardGen ml-4">
                    <select>
                      <option value="30"> últimos 30 dias</option>
                      <option value="60"> últimos 60 dias</option>
                      <option value="120"> últimos 120 dias</option>
                    </select>
                    <i className="icon-calendar"></i>
                </div>
              </div>
              <div className="wrapDatosTop">
                <div className="row">
                  <div className="col-6 col-md-3">
                    <div className="cardGen">
                      <div className="itemTop">
                        <i className="icon-puntos-v"></i>
                        <p>000</p>
                        <span>Tickets</span>
                      </div>
                    </div>
                  </div>

                  <div className="col-6 col-md-3">
                    <div className="cardGen">
                      <div className="itemTop">
                        <i className="icon-puntos-v"></i>
                        <p>000</p>
                        <span>Streaming</span>
                      </div>
                    </div>
                  </div>

                  <div className="col-6 col-md-3">
                    <div className="cardGen">
                      <div className="itemTop">
                        <i className="icon-puntos-v"></i>
                        <p>000</p>
                        <span>Merchandising</span>
                      </div>
                    </div>
                  </div>

                  <div className="col-6 col-md-3">
                    <div className="cardGen">
                      <div className="itemTop">
                        <i className="icon-puntos-v"></i>
                        <p>000</p>
                        <span>Donaciones</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="wrapActividad cardGen">
                <div className=""><p>Actividad</p></div>
                <div className="wrapGrafica">
                  <Actividad/>
                </div>
              </div>
              <div className="wrapVentas cardGen">
                <div className=""><p>Ventas por evento</p></div>
                <div className="wrapTabla">
                  <TablaVentas ventas={ventas}/>
                </div>
              </div>
            </div>
            <div className="col-12 col-lg-4">
              <div className="contentRight">
                <div className="cardGen">
                  <div className="totales">
                    <p className="totalD d-flex justify-content-center">
                      <small>$</small>
                      <span>000.000</span>
                      <small>USD</small>
                    </p>
                    <span>Total recaudado</span>
                    <div className="otras">
                      <p className="d-flex align-items-end">
                        <span>VEF</span>
                        <span></span>
                        <span>$ 00.00</span>
                      </p>

                      <p className="d-flex align-items-end">
                        <span>CLP</span>
                        <span></span>
                        <span>$ 00.00</span>
                      </p>

                      <p className="d-flex align-items-end">
                        <span>EUR</span>
                        <span></span>
                        <span>€ 00.00</span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
}

export default withTranslation()(Home)