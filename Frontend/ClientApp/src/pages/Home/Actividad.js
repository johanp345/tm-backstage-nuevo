import React from 'react';
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend,ResponsiveContainer} from 'recharts';


const data = [
    {name: '00', Donaciones: 4000, Streaming: 2400, Tickets: 2400},
    {name: '01', Donaciones: 3000, Streaming: 1398, Tickets: 2210},
    {name: '02', Donaciones: 2000, Streaming: 9800, Tickets: 2290},
    {name: '03', Donaciones: 2780, Streaming: 3908, Tickets: 2000},
    {name: '04', Donaciones: 1890, Streaming: 4800, Tickets: 3548},
    {name: '05', Donaciones: 2390, Streaming: 3800, Tickets: 7415},
    {name: '07', Donaciones: 3490, Streaming: 4300, Tickets: 3698},
    {name: '08', Donaciones: 4520, Streaming: 6587, Tickets: 5471},
    {name: '09', Donaciones: 6320, Streaming: 7412, Tickets: 1000},
    {name: '10', Donaciones: 3490, Streaming: 4300, Tickets: 2100},
    {name: '11', Donaciones: 2587, Streaming: 2547, Tickets: 6100},
];


export default ()=>{
    return (
        <ResponsiveContainer>
            <LineChart data={data}
                margin={{top: 5, right: 5, left: 5, bottom: 5}}>
                
                <XAxis dataKey="name"/>
                <YAxis/>
                <CartesianGrid stroke="#EFEFEF" vertical={false}/>
                <Tooltip/>
                <Legend />
                <Line strokeWidth="2" type="monotone" dataKey="Streaming" activeDot={false} stroke="#00D6CE" />
                <Line strokeWidth="2" type="monotone" dataKey="Donaciones" activeDot={false} stroke="#007FFF" />
                <Line strokeWidth="2" type="monotone" dataKey="Tickets" activeDot={false} stroke="#252BC3" />
            </LineChart>
        </ResponsiveContainer>
    );
}