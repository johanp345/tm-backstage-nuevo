import React, { useEffect } from 'react';
import { withTranslation } from 'react-i18next';

//const cookies = new Cookies();
const _404 = (props)=> {
  const {t} = props;
  useEffect(()=>{
    console.log(props);
  })
    return (
      <>
        <p>{t('titulo404')}</p>
      </>
    );
}

export default withTranslation()(_404)