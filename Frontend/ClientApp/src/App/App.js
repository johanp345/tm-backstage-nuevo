import React, {useEffect } from 'react';
import { Switch, Route,Redirect } from 'react-router-dom';
import { Layout } from '../components/Layout';
import Login from '../pages/Login';
import Reportes from '../pages/Reportes';
import Home from '../pages/Home';
import _404 from '../pages/404';
import "bootstrap";
import "../assets/css/main.scss";
import "../assets/css/theme.scss";
import "../assets/css/responsive.scss";
export default ({getGlobalConfig,setGlobalConfig,global})=> {
  //const login = true || cookies.get('username');
  let usuario = sessionStorage.getItem('usuario');

  useEffect(()=>{
    //console.log(props);
    getGlobalConfig();
    if(usuario){
      usuario=JSON.parse(usuario)
      usuario.UserCountry=JSON.parse(sessionStorage.getItem('user-country'));
      setGlobalConfig(usuario);
    }
  },[])
  
  useEffect(()=>{
    console.log(global);
  },[global])

    return (
      <>
        {
          (!global.Login && !usuario)?
              <Layout>
                <Switch>
                    <Route  exact path='/login/' component={Login} />
                    <Route  path='' render ={()=><Redirect to="/login/"/>} />
                </Switch>
              </Layout>
            :
            <Layout layout="menu">
              <Switch>
                <Route  path='/login/' render ={()=><Redirect to="/"/>}/>
                <Route exact path='/' component={Home} />
                <Route path='/reportes/:tipo?' component={Reportes} />
                <Route  path='' component={_404} />
              </Switch>
            </Layout>
        }
      </>
    );
}
