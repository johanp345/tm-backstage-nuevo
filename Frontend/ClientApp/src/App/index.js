import { connect } from "react-redux";
import App from "./App";

import { getGlobalConfig,setConfig } from "../actions/global/creators";

const mapStateToProps = (state) => ({
  global: state.global,
});

const mapDispatchToProps = (dispatch) => ({
  getGlobalConfig: (domain) => dispatch(getGlobalConfig(domain)),
  setGlobalConfig: (config) => dispatch(setConfig(config)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
