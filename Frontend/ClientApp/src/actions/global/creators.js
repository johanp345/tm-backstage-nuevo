import {
  startGetConfig,
  setGlobalConfig,
  errorGetConfig,
  setIdioma,
} from '.';
import API from '../../services/services';

export const getGlobalConfig = () => async (dispatch) => {
  try {
    dispatch(startGetConfig());
    const { status, data } = await API.config.getConfig();
    if (status === 200) {
      dispatch(setGlobalConfig(data));
    } else {
      dispatch(errorGetConfig('Error in get Config.'));
    }
  } catch (error) {
    console.log(error);
    dispatch(errorGetConfig('Error in get Config 2.'));
  }
};

export const setConfig = (config) => async (dispatch) => {
  try {
      dispatch(setGlobalConfig(config));
  } catch (error) {
    dispatch(errorGetConfig('Error in get Config 3.'));
  }
};

export const getIdioma = (idioma) => async (dispatch) => {
  try {
      dispatch(setIdioma(idioma));
  } catch (error) {
      dispatch(errorGetConfig('Error in get idioma.'));
  }
};
