import axios from 'axios';
import qs from 'qs';
const axiosInstanceAPI = axios.create({
    mode: 'cors'
});

const Services = {
    config:{
        getConfig:()=>
            axiosInstanceAPI.get(`/configClientApp.json`).catch((error)=>error.response),
    },
    access:{
        login: (url,data)=> 
            axiosInstanceAPI.post(url,qs.stringify(data),
                {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
                }
            ).catch((error)=>error.response),
    },
    filtros: {
        eventos: (baseUrl,data) =>
        axiosInstanceAPI.get(
            `montaje/evento/ObtenerTodosEventoPorUsuarioTodosDropDown?Usuario=${data.ID}&PaisID=${data.PaisID}&multiple=${data.SeleccionMultiple}`,
            
            {
              'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
              'baseURL':baseUrl
            }
          ),
        funciones: (baseUrl,data) =>
          axiosInstanceAPI.get(
            `montaje/funcion/FuncionesTotalesReportes?eventoID=${data.ID}&usuarioID=${data.UserID}&PaisID=${data.PaisID}`,
            
            {
              'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
              'baseURL':baseUrl
            }
          ).catch((error)=>error.response),
          eventosActivos: (baseUrl,data) =>
          axiosInstanceAPI.get(
            `montaje/evento/ObtenerTodosEventoPorUsuarioTodosDropDown?Usuario=${data.ID}&PaisID=${data.PaisID}&multiple=${data.SeleccionMultiple}`,
            
            {
              'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
              'baseURL':baseUrl
            }
          ).catch((error)=>error.response),
        funcionesActivos: (baseUrl,data) =>
          axiosInstanceAPI.get(
            `montaje/funcion/FuncionesTotalesReportes?eventoID=${data.ID}&usuarioID=${data.UserID}&PaisID=${data.PaisID}`,
            
            {
              'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
              'baseURL':baseUrl
            }
          ).catch((error)=>error.response),
        categorias: (baseUrl,data) =>
          axiosInstanceAPI.get(
            `Montaje/categoria/ObtenerParaDropdown?idioma=${data.Idioma}`,
            
            {
              'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
              'baseURL':baseUrl
            }
          ).catch((error)=>error.response),
      }
}

export default Services;